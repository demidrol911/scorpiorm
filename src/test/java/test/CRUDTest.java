package test;

import com.scorpio.orm.Session;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import test.models.SmurfEntity;


@DisplayName("Тест CRUD операций")
public class CRUDTest {
    
    private static Session session;
    private static Random random;
    
    @BeforeAll
    static void init() {
        session =  Session.open(new TestConfig(), true);
        random = new Random();
    }
    
    @DisplayName("Создание сущности")
    @Test
    void testCreate() throws SQLException {
        SmurfEntity smurf = new SmurfEntity();
        smurf.setName("Благоразумник");
        smurf.setAge(10);
        session.insert(smurf);
        
        List<SmurfEntity> smurfs = session.selectAll(SmurfEntity.class);
        assertEquals(smurf, smurfs.get(smurfs.size()-1));
    }
    
    @DisplayName("Создание коллекции сущностей")
    @Test
    void testCreateCollection() throws SQLException {
        SmurfEntity smurf1 = new SmurfEntity();
        smurf1.setName("Растяпа");
        smurf1.setAge(9);
        SmurfEntity smurf2 = new SmurfEntity();
        smurf2.setName("Ворчун");
        smurf2.setAge(13);
        List<SmurfEntity> smurfs = new ArrayList<>();
        smurfs.add(smurf1);
        smurfs.add(smurf2);
        session.insert(smurfs);
        
        List<SmurfEntity> smurfsFromDb = session.selectAll(SmurfEntity.class);
        assertEquals(smurf1, smurfsFromDb.get(smurfsFromDb.size()-2));
        assertEquals(smurf2, smurfsFromDb.get(smurfsFromDb.size()-1));
    }
    
    
    @DisplayName("Обновление сущности")
    @Test
    void testUpdate() throws SQLException {
        SmurfEntity smurf = session.selectById(SmurfEntity.class, 1);
        smurf.setAge(random.nextInt(20));
        session.update(smurf, "age");
        
        SmurfEntity smurfFromDb = session.selectById(SmurfEntity.class, 1);
        assertEquals(smurf, smurfFromDb);
    }
    
    
    @DisplayName("Обновление коллекции сущностей")
    @Test
    void testUpdateCollection() throws SQLException {
        SmurfEntity smurf1 = session.selectById(SmurfEntity.class, 1);
        smurf1.setAge(random.nextInt(20));
        SmurfEntity smurf2 = session.selectById(SmurfEntity.class, 2);
        smurf1.setAge(random.nextInt(20));
        List<SmurfEntity> smurfs = new ArrayList<>();
        smurfs.add(smurf1);
        smurfs.add(smurf2);
        session.update(smurfs, "age");
        
        SmurfEntity smurfFromDb1 = session.selectById(SmurfEntity.class, 1);
        SmurfEntity smurfFromDb2 = session.selectById(SmurfEntity.class, 2);
        assertEquals(smurf1, smurfFromDb1);
        assertEquals(smurf2, smurfFromDb2);
    }
    
    
    @DisplayName("Удаление сущности")
    @Test
    void testDelete() throws SQLException {
        List<SmurfEntity> smurfs = session.selectAll(SmurfEntity.class);
        SmurfEntity smurf = smurfs.get(smurfs.size()-1);
        session.delete(smurf);
        
        List<SmurfEntity> smurfsFromDb = session.selectAll(SmurfEntity.class);
        assertFalse(smurfsFromDb.contains(smurf));
    }
    
    
    @DisplayName("Удаление коллекции сущностей")
    @Test
    void testDeleteCollection() throws SQLException {
        List<SmurfEntity> smurfs = session.selectAll(SmurfEntity.class);
        List<SmurfEntity> smurfsForDelete = new ArrayList<>();
        smurfsForDelete.add(smurfs.get(smurfs.size()-1));
        smurfsForDelete.add(smurfs.get(smurfs.size()-2));
        session.delete(smurfsForDelete);
        
        List<SmurfEntity> smurfsFromDb = session.selectAll(SmurfEntity.class);
        assertFalse(smurfsFromDb.contains(smurfsForDelete.get(0)) || smurfsFromDb.contains(smurfsForDelete.get(1)));
    }
    
    @AfterAll
    static void destroy() throws SQLException {
        session.close();
    }
}
