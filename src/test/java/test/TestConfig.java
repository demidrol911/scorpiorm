package test;

import com.scorpio.orm.ConnectionConfig;
import javax.sql.DataSource;
import org.h2.jdbcx.JdbcDataSource;

/**
 * Конфигурация тестовой базы
 */
public class TestConfig implements ConnectionConfig {

    @Override
    public DataSource getDataSource() {
        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setUrl("jdbc:h2:./test");
        return dataSource;
    }

    @Override
    public String getUserName() {
        return "test";
    }

    @Override
    public String getPassword() {
        return "test";
    }
    
}
