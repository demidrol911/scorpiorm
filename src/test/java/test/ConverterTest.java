package test;

import com.scorpio.orm.converters.Converter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.time.LocalDate;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


@DisplayName("Тест конвертера типов")
public class ConverterTest {
    
    @DisplayName("Конвертация типов")
    @Test
    public void testConvertType() {
        assert 123 == Converter.convert("123", Integer.class);
        assert new BigDecimal(12.34).setScale(2, RoundingMode.HALF_UP).compareTo(Converter.convert("12.34", BigDecimal.class)) == 0;
        assert 123L == Converter.convert("123", Long.class);
        assert 12.34 == Converter.convert("12.34", Double.class);
        assert 12.34f == Converter.convert("12.34", Float.class);
        assert Boolean.TRUE == Converter.convert("1", Boolean.class);
        assert Boolean.FALSE == Converter.convert("0", Boolean.class);
        assert 123 == Converter.convert("123", Short.class);
        assert "123".equals(Converter.convert("123", String.class));
        assert LocalDate.of(2019, 7, 30).compareTo(Converter.convert("2019-07-30", LocalDate.class)) == 0;
        assert LocalDate.of(2019, 7, 30).compareTo(Converter.convert("30.07.2019", LocalDate.class)) == 0;
        assert "2019-07-30".equals(Converter.convert(LocalDate.of(2019, 7, 30), String.class));
        assert "12.34".equals(Converter.convert(new BigDecimal(12.34), String.class));
        assert "123".equals(Converter.convert(123, String.class));
        assert LocalDate.of(2019, 7, 30).
                compareTo(Converter.convert(Date.valueOf(LocalDate.of(2019, 7, 30)), LocalDate.class)) == 0;
    }
    
}
