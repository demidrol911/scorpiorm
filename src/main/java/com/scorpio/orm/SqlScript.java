package com.scorpio.orm;

/**
 * Описание Plain SQL-скрипта
 */
public interface SqlScript {
    public static enum Type {PLAIN, JSON};
    
    
    /** 
     * Получить имя скрипта. 
     * @return 
     */
    public String getName();
    
    
    /**
     * Получить тип данных, которые возвращает скрипт
     * @return 
     */
    default public Type getType(){ return Type.PLAIN; }
}
