package com.scorpio.orm;

import java.util.HashMap;


/**
 * Параметр
 */
public class Params {
    
    /** Имя параметра */
    private final String name;
    
    /** Значение */
    private final Object value;
    
    public Params(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public String getName() { return name; }

    public Object getValue() { return value; }
   
    /**
     * Конвертер массива праметров в HashMap
     * @param params массив параметров
     * @return HashMap
     */
    public static HashMap<String, Object> toMap(Params[] params) {
        HashMap<String, Object> paramsMap = new HashMap<>();
        for(Params p: params) {
            paramsMap.put(p.getName(), p.getValue());
        }
        return paramsMap;
    } 
}
