package com.scorpio.orm;

import com.scorpio.orm.annotations.Entity;
import com.scorpio.orm.annotations.Foreign;
import com.scorpio.orm.annotations.Id;
import com.scorpio.orm.converters.Converter;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Класс, обеспечивает взаимодействие с сущностью через механизм Reflection
 */
public final class EntityProvider {
    
    /** Провайдеры */
    private static final HashMap<Class, EntityProvider> PROVIDERS = new HashMap<>();
    
    /** Сущность */
    private Object entity;
    
    /** Метаданные о сущности */
    private final EntityMeta meta;
    
    /**
     * Получить провайдер
     * @param entity
     * @return 
     */
    public static EntityProvider getProvider(Object entity) {
        Class clazz = entity.getClass();
        if(!PROVIDERS.containsKey(clazz)) {
            PROVIDERS.put(clazz, new EntityProvider(clazz));
        }
        EntityProvider provider = PROVIDERS.get(clazz);
        provider.entity = entity;
        return provider;
    }
    
    /**
     * Получить провайдер
     * @param clazz
     * @return 
     */
    public static EntityProvider getProvider(Class clazz) {
        try {
            return getProvider(clazz.newInstance());
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    private EntityProvider(Class clazz) {
        this.meta = new EntityMeta(clazz);
        
        List<String> errors = this.meta.check();
        if(!errors.isEmpty()) {
            throw new RuntimeException("Нарушения соглашений в " + clazz.getName() + 
                " " + String.join("\n", errors));
        }
    }
    
    /**
     * Получить сущность
     * @return 
     */
    public Object getEntity() {
        return entity;
    }
    
    /**
     * Получить наименование таблицы в базе данных
     * @return 
     */
    public String getTableName() {
        return meta.entityAnnotation.table();
    }
    
    /**
     * Получить множество колонок в таблице в базе данных
     * @return 
     */
    public Set<String> getColumns() {
        return meta.columns.keySet();
    }
    
    /**
     * Установить значение полю
     * @param name наименование
     * @param value значение
     * @param session сессия
     */
    public void setValue(String name, Object value, Session session) {
        int index = meta.names.getOrDefault(name, -1);
        if(index == -1 && name.equals(getIdName())) {
            setId(value); 
        }
        else if(index > -1) {
            Field f = meta.fields.get(index);
            FieldMeta fm = meta.fieldsmeta.get(index);
           
            try {
                Object v = (value != null && f.isAnnotationPresent(Foreign.class) && !fm.isEntity(value.getClass())) ? 
                    session.selectByIdUsingCache(fm.type, value): Converter.convert(value, fm.type);
                if(fm.isCollection) {
                    if(f.get(entity) == null) {
                        f.set(entity, new ArrayList<>());
                    }
                    ((Collection)f.get(entity)).add(v);
                }
                else {
                    f.set(entity, v);
                }
            } catch (IllegalArgumentException | IllegalAccessException | SQLException ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }
        }
    }
    
    /**
     * Получить значение по имени поля
     * @param column имя поля
     * @param id признак для сущшости получить значение идентификатора
     * @return 
     */
    public Object getValue(String column, boolean id) {
        int idx = meta.names.get(column);
        Field f = meta.fields.get(idx);
        FieldMeta fm = meta.fieldsmeta.get(idx);
        try {
            Object v = f.get(entity);
            if(fm.isEntity && id) {
                return v != null? EntityProvider.getProvider(v).getId(): null;
            } else {
                return v;
            }
        } catch (IllegalArgumentException | IllegalAccessException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    /**
     * Получить тип поля
     * @param column имя поля
     * @return 
     */
    public Class getFieldType(String column) {
        int idx = meta.names.get(column);
        return meta.fieldsmeta.get(idx).type;
    }
  
    /**
     * Установить значение идентификатора
     * @param id 
     */
    public void setId(Object id) {
        Field f = meta.id;
        try {
            if(f != null && f.get(entity) == null) {
                f.set(entity, Converter.convert(id, f.getType()));
            }
        } catch (IllegalArgumentException | IllegalAccessException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    /**
     * Получить значение идентификатора
     * @return 
     */
    public Object getId() {
        Field f = meta.id;
        try {
            return f.get(entity);
        } catch (IllegalArgumentException | IllegalAccessException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    /**
     * Получить имя колонки идентификатора
     * @return 
     */
    public String getIdName() {
        Field f = meta.id;
        return f.getAnnotation(com.scorpio.orm.annotations.Field.class).column();
    }
    
    @Override
    public String toString() {
        return entity.toString();
    }
    
    /**
     * Метаданные о сущности
     */
    private class EntityMeta {
        
        /** Аннотация сущности */
        private final Entity entityAnnotation;
        
        /** Идентификатор */
        private final Field id;
        
        /** Поля сущности */
        private final List<Field> fields;
        
        /** Метаданные о полях */
        private final List<FieldMeta> fieldsmeta;
        
        /** Колонки в таблице */
        private final HashMap<String, Integer> columns;
        
        /** Теги в XML-файле */
        private final HashMap<String, Integer> tags;
        
        /** Отображение имен на поля */
        private final HashMap<String, Integer>  names;
        
        
        private EntityMeta(Class clazz) {
            this.entityAnnotation = (Entity) clazz.getDeclaredAnnotation(Entity.class);
            this.id = initId(clazz);
            this.fields = initFields(clazz);
            this.fieldsmeta = this.fields.stream().map(f -> new FieldMeta(f)).collect(Collectors.toList());
            
            this.columns = new HashMap<>();
            this.tags = new HashMap<>();
            this.names = new HashMap<>();
            
            for(int i = 0; i < this.fields.size(); i ++) {
                Field f = this.fields.get(i);
                 com.scorpio.orm.annotations.Field fa = 
                    f.getDeclaredAnnotation(com.scorpio.orm.annotations.Field.class);
                if(fa != null) {
                    this.columns.put(fa.column().isEmpty()? f.getName(): fa.column(), i);
                    if(!fa.tag().isEmpty()) {
                        this.tags.put(fa.tag(), i);
                    }
                }
                
                if(f.isAnnotationPresent(Foreign.class) || fa != null) {
                    this.names.put(f.getName(), i);
                }
                this.names.putAll(this.columns);
                this.names.putAll(this.tags);
                
            }
        }
        
        
        
        /**
         * Инициализировать идентификатор
         * @param clazz
         * @return 
         */
        private Field initId(Class clazz) {
            Field f = null;
            for(Field field: clazz.getDeclaredFields()) {
                if(f == null && field.isAnnotationPresent(Id.class)) {
                    f = field;
                    f.setAccessible(true);
                }
                else if(f != null && field.isAnnotationPresent(Id.class)) {
                    throw new RuntimeException("В классе не должно быть больше одного Id");
                }
            }
            return f;
        }
        
        
        /**
         * Инициализировать список полей, с которыми можно 
         * взаимодействовать через провайдер
         * @param clazz
         * @return 
         */
        private List<Field> initFields(Class clazz) {
            Class curClass = clazz;
            List<Field> fieldsList = new ArrayList<>();
            List<String> processedList = new ArrayList<>();
            while(curClass != null) {
                
                for(Field field: curClass.getDeclaredFields()) {
                    Id ia = field.getAnnotation(Id.class);
                    
                    if(!(Modifier.isStatic(field.getModifiers()) 
                        || (ia != null && ia.generate())
                        || processedList.contains(field.getName()))
                        && (field.isAnnotationPresent(com.scorpio.orm.annotations.Field.class) 
                            || field.isAnnotationPresent(Foreign.class))) {
                        
                        field.setAccessible(true);
                        fieldsList.add(field);
                        processedList.add(field.getName());
                    }
                }
                curClass = curClass.getSuperclass();
            }
            return fieldsList;
        }
        
        
        /**
         * Проверить класс сущности на соответствие соглашениям
         * @return 
         */
        public List<String> check() {
            List<String> errors = new ArrayList<>();
            if(id == null && !entityAnnotation.table().isEmpty()) {
                errors.add("Отсутствует аннотация Id");
            }

            if(id != null && !id.isAnnotationPresent(com.scorpio.orm.annotations.Field.class)) {
                errors.add("Поле с аннотацией Id, должно быть помечено аннотацией Field");
            }

            for(int i = 0; i < fields.size(); i++) {
                Field f = fields.get(i);
                FieldMeta fm = fieldsmeta.get(i);
                if((!fm.isEntity && f.isAnnotationPresent(Foreign.class)) || (fm.isEntity &&  !f.isAnnotationPresent(Foreign.class))) {
                    errors.add("Для " + f.getName() + " не должна быть указана аннотация @Foreign");
                }
            }
            return errors;

        }
    }
    
    
    /**
     * Метаданные о поле
     */
    class FieldMeta {
        
        /** Признак коллекции */
        private final boolean isCollection;
        
        /** Признак сущности */
        private final boolean isEntity;
        
        /** Тип поля */
        private final Class type;
        
        private FieldMeta(Field field) {
            Class clazz = field.getType();
            this.isCollection = Collection.class.isAssignableFrom(clazz);
            this.type = this.isCollection ? (Class)((ParameterizedType)field.getGenericType()).getActualTypeArguments()[0]: clazz;
            this.isEntity = isEntity(this.type);
        }
        
        final boolean isEntity(Class clazz) {
            return !(Number.class.isAssignableFrom(clazz) || String.class.equals(clazz) || Boolean.class.equals(clazz) 
                || LocalDate.class.equals(clazz) || LocalDateTime.class.equals(clazz));
        }
            
    }
}
