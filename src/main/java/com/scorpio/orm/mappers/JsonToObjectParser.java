package com.scorpio.orm.mappers;

import com.scorpio.orm.EntityProvider;
import com.scorpio.orm.Session;
import java.io.Reader;
import java.io.StringReader;
import java.util.Stack;
import javax.json.Json;
import javax.json.stream.JsonParser;

/**
 * Парсер JSON в объект
 */
public class JsonToObjectParser {
    
    private final Class clazz;

    public JsonToObjectParser(Class clazz) {
        this.clazz = clazz;
    }
    
    public <T> T parse(Reader jsonReader, Session s) {
        JsonParser parser = Json.createParser(jsonReader);
        Stack<Object> oStack = new Stack<>();
        Stack<String> arkeyStack = new Stack<>();
        String nextKey = null;
        EntityProvider provider = null;
       
        while(parser.hasNext()) {
            JsonParser.Event event = parser.next();
            switch(event) {
                case START_ARRAY:
                    arkeyStack.push(nextKey);
                    break;
                case START_OBJECT:
                    if(oStack.isEmpty()) {
                        provider = EntityProvider.getProvider(clazz);
                    }
                    else {
                        EntityProvider parent = EntityProvider.getProvider(oStack.peek());
                        String key = arkeyStack.isEmpty()? nextKey: arkeyStack.peek();
                        provider = EntityProvider.getProvider(parent.getFieldType(key));
                        parent.setValue(key, provider.getEntity(), s);
                    }
                    oStack.push(provider.getEntity());
                    break;
                case END_OBJECT:
                    if(oStack.size() > 1) {
                        oStack.pop();
                    }
                    break;
                case END_ARRAY:
                    arkeyStack.pop();
                    break;
                case KEY_NAME:
                    nextKey = parser.getString();
                    break;
                case VALUE_STRING:
                    provider.setValue(nextKey, parser.getString(), s);
                    break;
                case VALUE_NUMBER:
                    provider.setValue(nextKey, parser.isIntegralNumber()? parser.getLong(): parser.getBigDecimal(), s);
                    break;
                case VALUE_TRUE:
                    provider.setValue(nextKey, true, s);
                    break;
                case VALUE_FALSE:
                    provider.setValue(nextKey, false, s);
                    break;
                case VALUE_NULL:
                    provider.setValue(nextKey, null, s);
                    break;
                default: break;
            }
        }
        
        return oStack.size() == 1? (T)oStack.pop(): null;
    }
    
    public <T> T parse(String jsonString, Session s) {
        return parse(new StringReader(jsonString), s);
    }
}
