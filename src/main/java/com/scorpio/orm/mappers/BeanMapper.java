package com.scorpio.orm.mappers;

import com.scorpio.orm.EntityProvider;
import com.scorpio.orm.Session;
import com.scorpio.orm.converters.Converter;
import java.sql.Array;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Collection;

/**
 * Маппер результирующего набора ResultSet в Bean
 */
public class BeanMapper extends ResultSetMapper<Object> {
    
    private final Class clazz;
    
    public BeanMapper(Class clazz) {
        this.clazz = clazz;
    }
    
    @Override
    public Object map(ResultSet rs, Session s) throws SQLException{
        ResultSetMetaData resultMeta = rs.getMetaData();
        
        EntityProvider provider = EntityProvider.getProvider(clazz);
        for(int i = 1; i <= resultMeta.getColumnCount(); i++) {
            String column = resultMeta.getColumnName(i).toLowerCase();
            if(provider.getColumns().contains(column)) {
                Object obj = rs.getObject(column);
                if(obj != null) {
                    if(obj instanceof Array) {
                        Converter.convert(obj, Collection.class).forEach(item -> provider.setValue(column, item, s));
                    }
                    else {
                        provider.setValue(column, rs.getObject(column), s);
                    }
                }
            }
            else if (provider.getIdName().equals(column)) {
                provider.setId(rs.getObject(column));
            }
        }
        
        return provider.getEntity();  
        
    }
    
}
