package com.scorpio.orm.mappers;

import com.scorpio.orm.Session;
import com.scorpio.orm.converters.Converter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import org.postgresql.jdbc.PgArray;

/**
 * Маппер результирующего набора ResultSet в HashMap
 */
public class HashMapMapper extends ResultSetMapper<HashMap<String, Object>>{
    
    @Override
    public HashMap<String, Object> map(ResultSet rs, Session s) throws SQLException {
        ResultSetMetaData resultMeta = rs.getMetaData();
        HashMap<String, Object> rowMap = new HashMap<>();
        for(int i = 1; i <= resultMeta.getColumnCount(); i++) {
            String column = resultMeta.getColumnName(i);
            Object obj = rs.getObject(column);
            
            if(obj instanceof java.sql.Date) {
                obj = Converter.convert(obj, LocalDate.class);
            }
            else if(obj instanceof java.sql.Timestamp) {
                obj = Converter.convert(obj, LocalDateTime.class);
            }
            else if(obj instanceof PgArray) {
                obj = Converter.convert(obj, Collection.class);
            }
            
            rowMap.put(column, obj);
        }
        return rowMap;
    }
    
}
