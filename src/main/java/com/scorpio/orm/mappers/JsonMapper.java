package com.scorpio.orm.mappers;

import com.scorpio.orm.Session;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Маппер результирущего набора ResultSet в виде JSON в Bean
 */
public class JsonMapper extends ResultSetMapper<Object>{
    
    private final JsonToObjectParser jsonParser;
    
    public JsonMapper(Class clazz) {
        jsonParser = new JsonToObjectParser(clazz);
    }

    @Override
    public Object map(ResultSet rs, Session s) throws SQLException {
        return jsonParser.parse(rs.getCharacterStream(1), s);
    }
}
