package com.scorpio.orm.mappers;

import com.scorpio.orm.Session;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Маппер результирующего набора ResultSet в
 * @param <T>
 */
public abstract class ResultSetMapper<T> {
      
    public abstract <T> T map(ResultSet rs, Session s) throws SQLException;
}
