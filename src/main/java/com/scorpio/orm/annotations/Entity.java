package com.scorpio.orm.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
 * Сущность 
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Entity {
    
    /** Наименование на естественном языке
     * @return  
     */
    String name() default "";
    
    /** Таблица
     * @return  
     */
    String table() default "";
    
    /** Корневой тег в файле
     * @return  
     */
    String tagroot() default "";
    
    /** 
     * Комментарий (пояснение) 
     * @return 
     */
    String comment() default "";
}
