package com.scorpio.orm.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Внешняя ссылка
 * В случае если применяется для поля вместе с аннотацией Field,
 * то поле является внешним ключом. Такие поля нужны для перемещения 
 * на верхний уровень по иерархии объектов.
 * В случае если применяется без аннотации Field, то поле является 
 * ссылкой на вложенный объект или коллекцию вложенных объектов и 
 * отображение на поле базы данных не имеет. Такие поля нужны для
 * перемещения на нижний уровень по иерархии объектов
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Foreign {
    
}
