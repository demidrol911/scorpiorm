package com.scorpio.orm.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
 * Поле идентификатор 
 * Требует наличия аннотации Field у поля, для которого указывается
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Id {
    
    /** Идентификатор генерируется базой данных
     * @return  
     */
    boolean generate() default false;
}
