package com.scorpio.orm.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
 * Поле 
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Field {
    
    /** Наименование на естественном языке
     * @return  
     */
    String name() default "";
    
    /** Колонка в таблице
     * @return  
     */
    String column() default "";
    
    /** Тег в файле
     * @return  
     */
    String tag() default "";
    
    /** Комментарий (пояснение)
     * @return  
     */
    String comment() default "";
}
