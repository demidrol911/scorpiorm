package com.scorpio.orm.converters;

import java.time.LocalDate;

/**
 * Конвертер LocalDate в String
 */
public class LocalDateToString implements TypeConverterTo<LocalDate, String>{

    @Override
    public String convert(LocalDate value) {
        return value.format(StringToLocalDate.DEFAULT_FORMATTER);
    }
    
}
