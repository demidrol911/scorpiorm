package com.scorpio.orm.converters;

import java.math.BigDecimal;

/**
 * Конвертер Long в BigDecimal
 */
public class LongToBigDecimal implements TypeConverterTo<Long, BigDecimal>{

    @Override
    public BigDecimal convert(Long value) {
        return new BigDecimal(value);
    }
    
}
