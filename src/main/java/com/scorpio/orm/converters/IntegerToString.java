package com.scorpio.orm.converters;

/**
 * Конвертер Integer в String
 */
public class IntegerToString implements TypeConverterTo<Integer, String> {

    @Override
    public String convert(Integer value) {
        return String.valueOf(value);
    }
    
}
