package com.scorpio.orm.converters;

import java.sql.Date;
import java.time.LocalDate;

/**
 * Конвертер java.sql.Date в LocalDate
 */
public class SqlDateToLocalDate implements TypeConverterTo<Date, LocalDate>{

    @Override
    public LocalDate convert(Date value) {
        return value.toLocalDate();
    }
    
}
