package com.scorpio.orm.converters;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Конвертер BigDecimal в String
 */
public class BigDecimalToString implements TypeConverterTo<BigDecimal, String>{
    
    private static final DecimalFormat DECIMAL_FORMATTER = new DecimalFormat("#0.00");

    @Override
    public String convert(BigDecimal value) {
        return DECIMAL_FORMATTER.format(value).replace(",", ".");
    }
    
}
