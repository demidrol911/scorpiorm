package com.scorpio.orm.converters;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import org.postgresql.jdbc.PgArray;

/**
 * Конвертер org.postgresql.jdbc.PgArray в Collection
 */
public class PgArrayToCollection implements TypeConverterTo<PgArray, Collection>{

    @Override
    public Collection convert(PgArray value) {
        try {
            return Arrays.asList((Object[])value.getArray());
        } catch (SQLException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }     
    }
    
}
