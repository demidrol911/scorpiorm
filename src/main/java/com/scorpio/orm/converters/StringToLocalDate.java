package com.scorpio.orm.converters;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

/**
 * Конвертер String в LocalDate
 */
public class StringToLocalDate implements TypeConverterTo<String, LocalDate>{
    
    protected static final DateTimeFormatter DEFAULT_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    
    protected static final DateTimeFormatter DATE_FORMATTER = new DateTimeFormatterBuilder()
            .appendOptional(DEFAULT_FORMATTER)
            .appendOptional(DateTimeFormatter.ofPattern("dd.MM.yyyy"))
            .toFormatter();

    @Override
    public LocalDate convert(String value) {
        return LocalDate.parse(value, DATE_FORMATTER);
    }
    
}
