package com.scorpio.orm.converters;

import java.math.BigDecimal;

/**
 * Интерфейс конвертера из типа 
 * @param <S> в примитивные типы
 */
public interface TypeConverter<S> {
    
    public String toString(S value);
    
    public Boolean toBoolean(S value);
    
    public Double toDouble(S value);
    
    public Float toFloat(S value);
    
    public BigDecimal toDecimal(S value);
    
    public Integer toInteger(S value);
    
    public Long toLong(S value);
    
    public Short toShort(S value);
    
}
