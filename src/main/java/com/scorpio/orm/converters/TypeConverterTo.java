package com.scorpio.orm.converters;

/**
 * Интерфейс конвертера из одного типа
 * @param <S>
 * в другой
 * @param <D> 
 */
public interface TypeConverterTo<S, D> {
    
    public D convert(S value);
            
}
