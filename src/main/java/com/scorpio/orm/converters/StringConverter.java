package com.scorpio.orm.converters;

import java.math.BigDecimal;

/**
 * Конвертер String в примитивные типы
 */
public class StringConverter implements TypeConverter<String> {

    @Override
    public String toString(String value) {
        return value;
    }

    @Override
    public Boolean toBoolean(String value) {
        return value.equalsIgnoreCase("true") || value.equalsIgnoreCase("t")
            || value.equalsIgnoreCase("yes") || value.equalsIgnoreCase("y")
            || value.equals("1") || Boolean.valueOf(value);
    }

    @Override
    public Double toDouble(String value) {
        return Double.valueOf(value);
    }

    @Override
    public Float toFloat(String value) {
        return Float.valueOf(value);
    }

    @Override
    public BigDecimal toDecimal(String value) {
        return new BigDecimal(value);
    }

    @Override
    public Integer toInteger(String value) {
        return Integer.valueOf(value);
    }

    @Override
    public Long toLong(String value) {
        return Long.valueOf(value);
    }

    @Override
    public Short toShort(String value) {
        return Short.valueOf(value);
    }
    
}
