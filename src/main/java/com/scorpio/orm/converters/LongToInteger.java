package com.scorpio.orm.converters;

/**
 * Конвертер Long в Integer
 */
public class LongToInteger implements TypeConverterTo<Long, Integer>{

    @Override
    public Integer convert(Long value) {
        return value.intValue();
    }
    
}
