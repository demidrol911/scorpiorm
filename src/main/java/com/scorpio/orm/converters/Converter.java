package com.scorpio.orm.converters;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.HashMap;

/**
 * Конвертер типов
 */
public class Converter {
    
    /** Конвертеры */
    private static final HashMap<Class, TypeConverter> TYPE_CONVERTERS = new HashMap<>();
    private static final HashMap<Class, HashMap<Class, TypeConverterTo>> TYPE_TO_CONVERTERS = new HashMap<>();
    
    /** Здесь зарегистрированы все конвертеры по умолчанию */
    static {
        registerConverter(new StringConverter());
        registerConverter(new StringToLocalDate());
        registerConverter(new IntegerToString());
        registerConverter(new BigDecimalToString());
        registerConverter(new LocalDateToString());
        registerConverter(new SqlDateToLocalDate());
        registerConverter(new SqlTimestampToLocalDateTime());
        registerConverter(new LongToBigDecimal());
        registerConverter(new LongToInteger());
        registerConverter(new PgArrayToCollection());
    }
    
    /**
     * Конвертировать значение из одного типа в другой
     * @param <T>
     * @param value
     * @param type
     * @return 
     */
    public static <T> T  convert(Object value, Class<T> type) {
        if(value == null) {
            return null;
        }
        
        Class clazz = value.getClass();
        if(clazz.equals(type)) {
            return (T) value;
        }
        
        HashMap<Class, TypeConverterTo> cm = TYPE_TO_CONVERTERS.getOrDefault(clazz, null);
        if(cm != null) {
            TypeConverterTo c = cm.get(type);
            if(c != null) {
                return (T) c.convert(value);
            }
        }
        
        TypeConverter c = TYPE_CONVERTERS.get(clazz);
        if(c != null) {
            if(String.class.equals(type)) {
                return (T) c.toString();
            } else if(Boolean.class.equals(type)) {
                return (T) c.toBoolean(value);
            }
            else if(Double.class.equals(type)) {
                return (T) c.toDouble(value);
            }
            else if(Float.class.equals(type)) {
                return (T) c.toFloat(value);
            }
            else if(BigDecimal.class.equals(type)) {
                return (T) c.toDecimal(value);
            }
            else if(Integer.class.equals(type)) {
                return (T) c.toInteger(value);
            }
            else if(Long.class.equals(type)) {
                return (T) c.toLong(value);
            }
            else if(Short.class.equals(type)) {
                return (T) c.toShort(value);
            }
        }
       
        throw new RuntimeException("Нет конвертера из " + clazz.getName() + " в " + type.getName()); 
    }
    
    /**
     * Зарегистрировать конвертер
     * @param c 
     */
    public static void registerConverter(TypeConverter c) {
        Class clazz = c.getClass();
        Type[] interfaces = clazz.getGenericInterfaces();
        ParameterizedType type = null;
        if(interfaces.length == 1) {
            type = (ParameterizedType) interfaces[0];
        }
        if(type == null) {
            throw new RuntimeException("Конвертер " + clazz.getName() + " не может быть зарегистрирован");
        } 
        TYPE_CONVERTERS.put((Class) type.getActualTypeArguments()[0], c);
        
    }
    
    /**
     * Зарегистрировать конвертер
     * @param c 
     */
    public static void registerConverter(TypeConverterTo c) {
        Class clazz = c.getClass();
        Type[] interfaces = c.getClass().getGenericInterfaces();
        ParameterizedType type = null;
        if(interfaces.length == 1) {
            type = (ParameterizedType) interfaces[0];
        }
        if(type == null) {
            throw new RuntimeException("Конвертер " + clazz.getName() + " не может быть зарегистрирован");
        }
        Type[] typeParams = type.getActualTypeArguments();
        Class typeParam1 = (Class) typeParams[0];
        Class typeParam2 = (Class) typeParams[1];
        if(!TYPE_TO_CONVERTERS.containsKey(typeParam1)) {
            TYPE_TO_CONVERTERS.put(typeParam1, new HashMap<>());
        }
        TYPE_TO_CONVERTERS.get(typeParam1).put(typeParam2, c);
    }
}
