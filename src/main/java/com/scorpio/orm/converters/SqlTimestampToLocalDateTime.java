package com.scorpio.orm.converters;

import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Конвертер из java.sql.Timestamp в LocalDateTime
 */
public class SqlTimestampToLocalDateTime implements TypeConverterTo<Timestamp, LocalDateTime>{

    @Override
    public LocalDateTime convert(Timestamp value) {
        return value.toLocalDateTime();
    }
    
}
