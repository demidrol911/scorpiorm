package com.scorpio.orm;

import javax.sql.DataSource;


/** 
 * Интерфейс конфигурации соединения 
 */
public interface ConnectionConfig {
    
    public DataSource getDataSource();

    public String getUserName();
    
    public String getPassword();
    
}
