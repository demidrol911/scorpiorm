package com.scorpio.orm;

import java.sql.SQLException;
import java.util.HashMap;

/**
 * Кэш
 */
class Cache {

    private final HashMap<Class, HashMap<Object, Object>> entities;
    
    Cache() {
        entities = new HashMap<>();
    }
    
    /**
     * Получить объект из кэша
     * @param clazz
     * @param id
     * @param session
     * @return
     * @throws SQLException 
     */
    Object get(Class clazz, Object id, Session session) throws SQLException {
        if(!entities.containsKey(clazz)) {
            entities.put(clazz, new HashMap<>());
        }
        
        Object obj = entities.get(clazz).get(id);
        if(obj == null) {
            obj = session.selectById(clazz, id);
            if(obj != null) {
                entities.get(clazz).put(id, obj);
            }
        }
        
        return obj;
    }
}
