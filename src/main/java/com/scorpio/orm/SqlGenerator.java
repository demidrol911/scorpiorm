package com.scorpio.orm;

import java.io.StringWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

/**
 * Генератор SQL-команд
 */
public class SqlGenerator {
    
    private static final VelocityEngine VELOCITY_ENGINE = new VelocityEngine();
    static {
        VELOCITY_ENGINE.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        VELOCITY_ENGINE.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
    }
    
    
    private SqlGenerator() {}
    
    
    /**
     * Генерировать команду INSERT
     * @param provider провайдер
     * @return SQL INSERT
     */
    public static String insertCommand(EntityProvider provider) {
        StringBuilder cmd = new StringBuilder();
        Set<String> colums = provider.getColumns();
        cmd.append("INSERT INTO ")
        .append(provider.getTableName())
        .append(" (")
        .append(String.join(",", colums))
        .append(") VALUES (")
        .append(String.join(",", Collections.nCopies(colums.size(), "?")))
        .append(")");
        return cmd.toString();
    }
    
    
    /**
     * Генерировать команду UPDATE
     * @param provider провайдер
     * @param modifiedFields изменяемые поля
     * @return SQL UPDATE
     */
    public static String updateCommand(EntityProvider provider, Set modifiedFields) {
        StringBuilder cmd = new StringBuilder();
        cmd.append("UPDATE ")
        .append(provider.getTableName())
        .append(" SET ")
        .append(String.join("=?,", modifiedFields))
        .append("=? WHERE ")
        .append(provider.getIdName())
        .append("=?");
        return cmd.toString();
    }
    
    
    /**
     * Генерировать команду SELECT ALL
     * @param provider
     * @return SQL SELECT ALL
     */
    public static String selectAll(EntityProvider provider) {
        StringBuilder cmd = new StringBuilder();
        cmd.append("SELECT * FROM ")
        .append(provider.getTableName());
        return cmd.toString();
    }
    
    
    /**
     * Генерировать команду SELECT по идентификатору
     * @param provider
     * @return SQL SELECT
     */
    public static String selectById(EntityProvider provider) {
        StringBuilder cmd = new StringBuilder();
        cmd.append("SELECT * FROM ")
        .append(provider.getTableName())
        .append(" WHERE ")
        .append(provider.getIdName())
        .append("=?");
        return cmd.toString();
    }
    
    
    /**
     * Генерировать SQL-скрипт по шаблону Apache Velocity
     * @param scriptName имя скрипта
     * @param params параметры
     * @return SQL SELECT
     */
    public static String selectQueryFromScript(String scriptName, HashMap<String, Object> params) {
        VelocityContext context = new VelocityContext();
        params.entrySet().forEach(p -> { context.put(p.getKey(), p.getValue()); });
        StringWriter query = new StringWriter();
        VELOCITY_ENGINE.mergeTemplate("sql/"+scriptName+".sql", "utf-8", context, query);
        return query.toString();
    }
    
    
    /** 
     * Генерировать команду DELETE
     * @param provider
     * @return 
     */
    public static String deleteCommand(EntityProvider provider) {
        StringBuilder cmd = new StringBuilder();
        cmd.append("DELETE FROM ")
        .append(provider.getTableName())
        .append(" WHERE ")
        .append(provider.getIdName())
        .append("=?");
        return cmd.toString();
    }
}
