package com.scorpio.orm;

import com.scorpio.orm.mappers.BeanMapper;
import com.scorpio.orm.mappers.HashMapMapper;
import com.scorpio.orm.mappers.JsonMapper;
import com.scorpio.orm.mappers.ResultSetMapper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Сессия с базой данных
 */
public class Session extends ConnectionWrapper {
    
    /** Кэш объектов справочников */
    private Cache cache;
    
    private Session(Connection connection, boolean caching) {
        super(connection);
        if(caching) {
            cache = new Cache();
        }
    }
    
    
    public static Session open(Connection connection) {
        return new Session(connection, true);
    }
    
 
    /**
     * Открыть сессию
     * @param config конфигурация
     * @param autoCommit признак автоматической фиксации
     * @param caching включить кэширование
     * @return сессия
     */
    public static Session open(ConnectionConfig config, boolean autoCommit, boolean caching) {
        try {
            Session session = new Session(config.getDataSource()
                .getConnection(config.getUserName(), config.getPassword()), caching);
            session.setAutoCommit(autoCommit);
            return session;
        } catch(SQLException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    
    /**
     * Открыть сессию
     * @param config конфигурация
     * @param caching включить кэширование
     * @return сессия
     */
    public static Session open(ConnectionConfig config, boolean caching) {
        return open(config, true, caching);
    }
    
    
    /**
     * Выбрать данные SQL-скриптом в виде списка объектов
     * @param <T>
     * @param clazz класс
     * @param script скрипт
     * @param params HashMap параметров
     * @return список объектов
     * @throws SQLException 
     */
    public <T> List<T> select(Class<T> clazz, SqlScript script, HashMap<String, Object> params) throws SQLException {
        try(PreparedStatement statement = prepareStatement(SqlGenerator.selectQueryFromScript(script.getName(), params))) {
            ResultSetMapper mapper;
            if(script.getType().equals(SqlScript.Type.JSON)) {
                mapper = new JsonMapper(clazz);
            }
            else {
                mapper = new BeanMapper(clazz);
            }
            return select(statement, mapper, false);
        }
    }
    
    
    /**
     * Выбрать данные SQL-скриптом в виде списка объектов
     * @param <T>
     * @param clazz класс
     * @param script скрипт
     * @param params параметры
     * @return список объектов
     * @throws SQLException 
     */
    public <T> List<T> select(Class<T> clazz, SqlScript script, Params... params) throws SQLException {
        return select(clazz, script, Params.toMap(params));
    }
    
    
    /**
     * Выбрать данные SQL-скриптом в виде списка HashMap
     * @param script скрипт
     * @param params HashMap параметров
     * @return список HashMap
     * @throws SQLException 
     */
    public List<HashMap<String, Object>> select(SqlScript script, HashMap<String, Object> params) throws SQLException {
        try(PreparedStatement statement = prepareStatement(SqlGenerator.selectQueryFromScript(script.getName(), params))){
            return select(statement, new HashMapMapper(), false);
        }
    }
   
    
    /**
     * Выбрать данные SQL-скриптом в виде списка HashMap
     * @param script скрипт
     * @param params параметры
     * @return список HashMap
     * @throws SQLException 
     */
    public List<HashMap<String, Object>> select(SqlScript script, Params... params) throws SQLException {
        return select(script, Params.toMap(params));
    }
      
    /**
     * Выбрать все данные в виде списка объектов
     * @param <T>
     * @param clazz класс
     * @return список объектов
     * @throws SQLException 
     */
    public <T> List<T> selectAll(Class<T> clazz) throws SQLException {
        try(PreparedStatement statement = prepareStatement(SqlGenerator.selectAll(EntityProvider.getProvider(clazz)))) {
            return select(statement, new BeanMapper(clazz), false);
        }
    }
    
    /**
     * Выбрать объект по идентификатору
     * @param <T>
     * @param <X>
     * @param clazz класс
     * @param id идентификатор
     * @return объект
     * @throws SQLException 
     */
    public <T, X> T selectById(Class<T> clazz, X id) throws SQLException {
        try(PreparedStatement statement = prepareStatement(SqlGenerator.selectById(EntityProvider.getProvider(clazz)))) {
            statement.setObject(1, id);
            return (T) select(statement, new BeanMapper(clazz), true).get(0);
        }
    }
    
    
    /**
     * Выбрать объект по идентификатору, используя кэширование
     * @param <T>
     * @param <X>
     * @param clazz класс
     * @param id идентификатор
     * @return объект
     * @throws java.sql.SQLException
     */
    public <T, X> T selectByIdUsingCache(Class<T> clazz, X id) throws SQLException {
        if(cache != null) {
            return (T)cache.get(clazz, id, this);
        }
        else {
            return this.selectById(clazz, id);
        }
    }
    
    
    /**
     * Выбрать данные
     * @param <T>
     * @param statement запрос
     * @param mapper маппер
     * @param single значение должно быть единственное
     * @return
     * @throws SQLException 
     */
    private <T> List<T> select(PreparedStatement statement, ResultSetMapper mapper, boolean single) throws SQLException {
        try(ResultSet resultSet = statement.executeQuery()) {
            List<T> result = new ArrayList<>();
            while(resultSet.next()) {
                result.add((T) mapper.map(resultSet, this));
                if(single) {
                    break;
                }
            }
            if(single && resultSet.next()) {
                throw new SQLException("Нет может быть больше одного выбранного объекта "+statement);
            }
            if(single && result.isEmpty()) {
                throw new SQLException("Нет выбранного объекта "+ statement);
            }
            return result;
        }
    }
    
    
    /**
     * Вставить объект
     * @param object
     * @return
     * @throws SQLException 
     */
    public int insert(Object object) throws SQLException {
        EntityProvider provider = EntityProvider.getProvider(object);
        int executeResult;
        try (PreparedStatement statement = prepareStatement(SqlGenerator.insertCommand(provider), 
                Statement.RETURN_GENERATED_KEYS)) {
            
            int idx = 1;
            for(String column: provider.getColumns()) {
                statement.setObject(idx, provider.getValue(column, true));
                idx ++;
            }   
            executeResult = statement.executeUpdate();
            
            try(ResultSet id = statement.getGeneratedKeys()) {
                id.next();
                provider.setId(id.getObject(1));
            }
        }
        return executeResult;
    }
    
    
    /**
     * Вставить коллекцию объектов
     * @param objects
     * @return количество вставленных записей
     * @throws SQLException 
     */
    public int insert(Collection objects) throws SQLException {
        if(objects == null || objects.isEmpty()) {
            return 0;
        }
        Iterator it = objects.iterator();
        EntityProvider provider = EntityProvider.getProvider(it.next());
        int[] executeResult;
        try (PreparedStatement statement = prepareStatement(SqlGenerator.insertCommand(provider), 
                Statement.RETURN_GENERATED_KEYS)) {
            it = objects.iterator();
            while(it.hasNext()) {
                provider = EntityProvider.getProvider(it.next());
                int idx = 1;
                for(String column: provider.getColumns()) {
                    statement.setObject(idx, provider.getValue(column, true));
                    idx ++;
                }
                statement.addBatch();
            }   
            
            executeResult = statement.executeBatch();
            
            try(ResultSet ids = statement.getGeneratedKeys()) {
                it = objects.iterator();
                while(it.hasNext()) {
                    provider = EntityProvider.getProvider(it.next());
                    ids.next();
                    provider.setId(ids.getObject(1));
                }
            }
        }
        return executeResult.length;
    }
    
    
    /**
     * Обновить объект
     * @param object
     * @param modifiedFields измененные поля
     * @return 
     * @throws SQLException 
     */
    public int update(Object object, String... modifiedFields) throws SQLException {
        EntityProvider provider = EntityProvider.getProvider(object);
        Set<String> columns = modifiedFields.length > 0 
            ? Arrays.stream(modifiedFields).collect(Collectors.toSet())
            : provider.getColumns();
        try (PreparedStatement statement = prepareStatement(SqlGenerator.updateCommand(provider, columns))) {
            int idx = 1;
            for(String column: columns) {
                statement.setObject(idx, provider.getValue(column, true));
                idx ++;
            }   
            statement.setObject(idx, provider.getId());
            return statement.executeUpdate();
        }
    }
    
    
    /**
     * Обновить коллекцию объектов
     * @param objects
     * @param modifiedFields измененные поля
     * @return количество обновленных записей
     * @throws SQLException 
     */
    public int update(Collection objects, String... modifiedFields) throws SQLException {
        if(objects == null || objects.isEmpty()) {
            return 0;
        }
        Iterator it = objects.iterator();
        EntityProvider provider = EntityProvider.getProvider(it.next());
        Set<String> columns = modifiedFields.length > 0 
            ? Arrays.stream(modifiedFields).collect(Collectors.toSet())
            : provider.getColumns();
        try (PreparedStatement statement = prepareStatement(SqlGenerator.updateCommand(provider, columns))) {
            it = objects.iterator();
            while(it.hasNext()) {
                provider = EntityProvider.getProvider(it.next());
                int idx = 1;
                for(String column: columns) {
                    statement.setObject(idx, provider.getValue(column, true));
                    idx ++;
                }
                statement.setObject(idx, provider.getId());
                statement.addBatch();
            }   
            return statement.executeBatch().length;
        }
        
    }
    
    
    /**
     * Удалить объект
     * @param object
     * @return 
     * @throws java.sql.SQLException 
     */
    public int delete(Object object) throws SQLException {
        EntityProvider provider = EntityProvider.getProvider(object);
        try (PreparedStatement statement = prepareStatement(SqlGenerator.deleteCommand(provider))) {
            statement.setObject(1, provider.getId());
            return statement.executeUpdate();
        }
    }
    
    
    /**
     * Удалить коллекцию объектов
     * @param objects
     * @return 
     * @throws java.sql.SQLException
     */
    public int delete(Collection objects) throws SQLException {
        if(objects == null || objects.isEmpty()) {
            return 0;
        }
        Iterator it = objects.iterator();
        EntityProvider provider = EntityProvider.getProvider(it.next());
        try (PreparedStatement statement = prepareStatement(SqlGenerator.deleteCommand(provider))) {
            it = objects.iterator();
            while(it.hasNext()) {
                provider = EntityProvider.getProvider(it.next());
                statement.setObject(1, provider.getId());
                statement.addBatch();
            }
            return statement.executeBatch().length;
        }
    }
}
